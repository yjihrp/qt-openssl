﻿#include "qaesservice.h"

QAesService::QAesService(QObject *parent) : QObject(parent){}

QByteArray QAesService::encrypt(const QByteArray plain,const QByteArray key,const QByteArray iv,bool base64){
    QByteArray cipher;
    /* A 256 bit key */
    auto aes_key = reinterpret_cast<const unsigned char*>(key.data());
    /* A 128 bit IV */
    auto aes_iv = reinterpret_cast<const unsigned char*>(iv.data());
    /* Message to be encrypted */
    auto plain_buffer = reinterpret_cast<const unsigned char*>(plain.data());

    EVP_CIPHER_CTX *ctx;
    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())){
        ERR_print_errors_fp(stderr);
        return cipher;
    }

    /*
     * Initialise the encryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, aes_key, aes_iv)){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        return cipher;
    }

    int len = 0;
    int cipher_len = 0;
    /*
    * Buffer for ciphertext. Ensure the buffer is long enough for the
    * ciphertext which may be longer than the plaintext, depending on the
    * algorithm and mode.
    */

    auto cb_len = static_cast<size_t>(plain.count() + AES_BLOCK_SIZE);
    auto cipher_buffer = reinterpret_cast<unsigned char*>(malloc(cb_len));
    memset(cipher_buffer,0,cb_len);

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    if(1 != EVP_EncryptUpdate(ctx, cipher_buffer, &len, plain_buffer, plain.count())){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        free(cipher_buffer);
        return cipher;
    }

    cipher_len = len;

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if(1 != EVP_EncryptFinal_ex(ctx, cipher_buffer + len, &len)){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        free(cipher_buffer);
        return cipher;
    }
    cipher_len += len;

    // convert qt data
    cipher = QByteArray(reinterpret_cast<char*>(cipher_buffer), cipher_len);
    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);
    free(cipher_buffer);

    if(base64){cipher = cipher.toBase64();}
    return cipher;
}

QByteArray QAesService::decrypt(const QByteArray cipher,const QByteArray key,const QByteArray iv){
    QByteArray plain;
    /* A 256 bit key */
    auto aes_key = reinterpret_cast<const unsigned char*>(key.data());
    /* A 128 bit IV */
    auto aes_iv = reinterpret_cast<const unsigned char*>(iv.data());

    EVP_CIPHER_CTX *ctx;
    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())){
        ERR_print_errors_fp(stderr);
        return plain;
    }

    /*
     * Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, aes_key, aes_iv)){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        return plain;
    }

    auto cipher_buffer = reinterpret_cast<const unsigned char*>(cipher.data());

    int temp_len = 0;
    int plain_len = 0;
    auto cb_len = static_cast<size_t>(cipher.count());
    auto plain_buffer = reinterpret_cast<unsigned char*>(malloc(cb_len));
    memset(plain_buffer,0,cb_len);

    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    if(1 != EVP_DecryptUpdate(ctx, plain_buffer, &temp_len, cipher_buffer, cipher.count())){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        free(plain_buffer);
        return plain;
    }
    plain_len = temp_len;

    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plain_buffer + temp_len, &temp_len)){
        ERR_print_errors_fp(stderr);
        EVP_CIPHER_CTX_free(ctx);
        free(plain_buffer);
        return plain;
    }
    plain_len += temp_len;

    // convert qt data
    plain = QByteArray(reinterpret_cast<char*>(plain_buffer), plain_len);
    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);
    free(plain_buffer);
    return plain;
}
