INCLUDEPATH+= $$PWD

dep=$$PWD

# mingw
win32-g++{
    dep=$$PWD/mingw

    # 添加 openssl 库依赖
    LIBS += libcrypto
    LIBS += libssl

    # 添加引用路径
    INCLUDEPATH+= $$PWD/mingw/include
}

# msvc
win32-msvc{
    dep=$$PWD/msvc

    # 添加 openssl 库依赖
    LIBS += libcrypto.lib
    LIBS += libssl.lib

    # 添加引用路径
    INCLUDEPATH+= $$PWD/msvc/include
}

# linux
linux-g++{
    # 添加 openssl 库依赖

    LIBS += -lcrypto
    LIBS += -lssl
    LIBS += -ldl

    # QT_ARCH arm64
    contains(QT_ARCH,arm64){
        dep=$$PWD/aarch
        # 添加引用路径
        INCLUDEPATH+= $$PWD/aarch/include
    }
    # QT_ARCH mips64
    contains(QT_ARCH,mips64){
        dep=$$PWD/mips
        # 添加引用路径
        INCLUDEPATH+= $$PWD/mips/include
    }
    # QT_ARCH x86_64
    contains(QT_ARCH,x86_64){
        dep=$$PWD/amd
        # 添加引用路径
        INCLUDEPATH+= $$PWD/amd/include
}
    # QT_ARCH loongarch64
    contains(QT_ARCH,loongarch64){
        dep=$$PWD/loongarch64
        # 添加引用路径
        INCLUDEPATH+= $$PWD/loongarch64/include
    }
}
# QT_ARCH x64:x86_64;x32:i386;mips64;arm64;loongarch64
contains(QT_ARCH,i386) {
    dep=$$dep/x32
}
contains(QT_ARCH,x86_64) {
    dep=$$dep/x64
}
contains(QT_ARCH,arm64) {
    dep=$$dep/x64
}
contains(QT_ARCH,mips64) {
    dep=$$dep/x64
}
contains(QT_ARCH,loongarch64) {
    dep=$$dep/x64
}

CONFIG(debug, debug|release) {
    DEPENDPATH += $$dep/debug/bin
    QMAKE_LIBDIR += $$dep/debug/bin

    DEPENDPATH += $$dep/debug/lib
    QMAKE_LIBDIR += $$dep/debug/lib
}

CONFIG(release, debug|release) {
    DEPENDPATH += $$dep/release/bin
    QMAKE_LIBDIR += $$dep/release/bin

    DEPENDPATH += $$dep/release/lib
    QMAKE_LIBDIR += $$dep/release/lib
}

HEADERS += \
    $$PWD/qaesservice.h \
    $$PWD/qrsaservice.h

SOURCES += \
    $$PWD/qaesservice.cpp \
    $$PWD/qrsaservice.cpp
