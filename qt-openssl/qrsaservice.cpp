﻿#include "qrsaservice.h"

QRsaService::QRsaService(QObject *parent) : QObject(parent){}

bool QRsaService::generate(QByteArray& pub,QByteArray& pri,int bits,int pub_pem_ver,int pri_pem_ver,bool save) {
    int ret = 0;
    RSA* rsa = nullptr;
    BIGNUM* bne = nullptr;
    BIO* bp_public = nullptr;
    BIO* bp_private = nullptr;

    // 1. generate rsa key
    bne = BN_new();
    ret = BN_set_word(bne,RSA_F4);
    if (ret != 1) {
        qDebug() << __FUNCTION__ << "BN_set_word error" << ret;
        BN_free(bne);
        return false;
    }

    rsa = RSA_new();
    ret = RSA_generate_key_ex(rsa, bits, bne, nullptr);
    if (ret != 1) {
        qDebug() << __FUNCTION__ << "RSA_generate_key_ex error" << ret;
        RSA_free(rsa);
        BN_free(bne);
        return false;
    }

#ifndef QT_NO_DEBUG
    // RSA_print_fp(stdout, rsa, 0);
#endif

    auto pub_pem_name = "public.pem";
    auto pri_pem_name = "private.pem";

    // 2. save public key
    bp_public = BIO_new_file(pub_pem_name, "w+");
    if(pub_pem_ver <= 0){
        // PKCS#1
        // -----BEGIN RSA PUBLIC KEY-----
        ret = PEM_write_bio_RSAPublicKey(bp_public, rsa);
    }else{
        // PKCS#8
        // -----BEGIN PUBLIC KEY-----
        ret = PEM_write_bio_RSA_PUBKEY(bp_public, rsa);
    }
    if (ret != 1) {
        if(pub_pem_ver <= 0){
            qDebug() << __FUNCTION__ << "PEM_write_bio_RSAPublicKey error" << ret;
        }else{
            qDebug() << __FUNCTION__ << "PEM_write_bio_RSA_PUBKEY error" << ret;
        }
        if(QFile::exists(pub_pem_name)){
            QFile::remove(pub_pem_name);
        }
        BIO_free_all(bp_public);
        RSA_free(rsa);
        BN_free(bne);
        return false;
    }

    // 3. save private key
    bp_private = BIO_new_file("private.pem", "w+");
    if(pri_pem_ver == 0){
        // PKCS#1
        // -----BEGIN RSA PRIVATE KEY-----
        ret = PEM_write_bio_RSAPrivateKey(bp_private, rsa, nullptr, nullptr, 0, nullptr, nullptr);
    }else{
        // TODO test
        //ret = PEM_write_bio_PrivateKey(bp_private, rsa, nullptr, nullptr, 0, nullptr, nullptr);
    }
    if(ret != 1){
        if(pri_pem_ver == 0){
            qDebug() << __FUNCTION__ << "PEM_write_bio_RSAPrivateKey error" << ret;
        }else{

        }
    }

    // 4. free
    BIO_free_all(bp_public);
    BIO_free_all(bp_private);
    RSA_free(rsa);
    BN_free(bne);

    // 5.read pem file
    if(ret == 1){
        try {
            if(QFile::exists(pub_pem_name)){
                QFile fd(pub_pem_name);
                if(!fd.open(QIODevice::ReadOnly)){
                    return false;
                }
                pub = fd.readAll();
                fd.close();
                if(!save){fd.remove();}
            }

            if(QFile::exists(pri_pem_name)){
                QFile fd(pri_pem_name);
                if(!fd.open(QIODevice::ReadOnly)){
                    return false;
                }
                pri = fd.readAll();
                fd.close();
                if(!save){fd.remove();}
            }
        } catch (...) {
            qDebug() << "read pem file errror";
        }
    }

    return (ret == 1);
}

QByteArray QRsaService::pem(QString file){
    QByteArray content;
    if(QFile::exists(file)){
        QFile fd(file);
        if(!fd.open(QIODevice::ReadOnly)){
            return content;
        }
        content = fd.readAll();
        fd.close();
    }
    return content;
}

RSA* QRsaService::publicPem(QByteArray pem){
    RSA* rsa = nullptr;
    if(pem.isNull()||pem.isEmpty()){
        return rsa;
    }
    auto pkcs1 = publicPkcs1Pem(pem);
    auto pkcs8 = publicPkcs8Pem(pem);
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_puts(bio,pem.data());
    if(pkcs1){
        rsa = PEM_read_bio_RSAPublicKey(bio,nullptr,nullptr,nullptr);
    }
    if(pkcs8){
        rsa = PEM_read_bio_RSA_PUBKEY(bio,nullptr,nullptr,nullptr);
    }
    if(bio){BIO_free(bio);}
    return rsa;
}

RSA* QRsaService::privatePem(QByteArray pem){
    RSA* rsa = nullptr;
    if(pem.isNull()||pem.isEmpty()){
        return rsa;
    }
    auto pkcs1 = privatePkcs1Pem(pem);
    auto pkcs8 = privatePkcs8Pem(pem);
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_puts(bio,pem.data());
    if(pkcs1){
        rsa = PEM_read_bio_RSAPrivateKey(bio,nullptr,nullptr,nullptr);
    }
    if(pkcs8){
        // TODO
        // PEM_read_bio_PrivateKey()
        // rsa = PEM_read_bio_RSA_PUBKEY(bio,nullptr,nullptr,nullptr);
    }
    if(bio){BIO_free(bio);}
    return rsa;
}

QByteArray QRsaService::encrypt(const QByteArray pub_pem,const QByteArray plain,int padding,bool base64){
    auto rsa = publicPem(pub_pem);
    auto cipher = encrypt(rsa,plain,padding,base64);
    if(rsa){RSA_free(rsa);}
    return cipher;
}

QByteArray QRsaService::encrypt(RSA* rsa,const QByteArray plain,int padding,bool base64){
    QByteArray cipher;
    if(!rsa){
        return cipher;
    }
    if(plain.isNull()||plain.isEmpty()){
        return cipher;
    }
    auto plain_len = plain.length();
    auto plain_buffer = reinterpret_cast<const unsigned char*>(plain.data());
    auto rsa_size = static_cast<size_t>(RSA_size(rsa));
    auto cipher_buffer = reinterpret_cast<unsigned char*>(malloc(rsa_size));
    memset(cipher_buffer,0,rsa_size);
    auto ret = RSA_public_encrypt(plain_len,plain_buffer,cipher_buffer,rsa,padding);
    if(ret == -1){
        qCritical() << __FUNCTION__ << "could not encrypt: " << ERR_error_string(ERR_get_error(),nullptr);
        free(cipher_buffer);
        return cipher;
    }
    cipher = QByteArray(reinterpret_cast<char*>(cipher_buffer), ret);
    free(cipher_buffer);
    if(base64){cipher = cipher.toBase64();}
    return cipher;
}

QByteArray QRsaService::decrypt(const QByteArray pri_pem,const QByteArray cipher,int padding){
    auto rsa = privatePem(pri_pem);
    auto plain = decrypt(rsa,cipher,padding);
    if(rsa){RSA_free(rsa);}
    return plain;
}

QByteArray QRsaService::decrypt(RSA* rsa,const QByteArray cipher,int padding){
    QByteArray plain;
    if(!rsa){
        return plain;
    }
    if(cipher.isNull()||cipher.isEmpty()){
        return plain;
    }
    auto cipher_len = RSA_size(rsa);
    auto cipher_buffer = reinterpret_cast<const unsigned char*>(cipher.constData());
    auto rsa_size = static_cast<size_t>(RSA_size(rsa));
    auto plain_buffer =  reinterpret_cast<unsigned char*>(malloc(rsa_size));
    memset(plain_buffer,0,rsa_size);
    int ret = RSA_private_decrypt(cipher_len,cipher_buffer,plain_buffer,rsa,padding);
    if(ret == -1){
        qCritical() << __FUNCTION__ << "could not decrypt: " << ERR_error_string(ERR_get_error(),nullptr);
        free(plain_buffer);
        return plain;
    }
    plain = QByteArray(reinterpret_cast<char*>(plain_buffer),ret);
    free(plain_buffer);
    return plain;
}

QByteArray QRsaService::sign(const QByteArray pri_pem,const QByteArray plain,int algorithm,bool base64){
    auto rsa = privatePem(pri_pem);
    auto signature = sign(rsa,plain,algorithm,base64);
    if(rsa){RSA_free(rsa);}
    return signature;
}

QByteArray QRsaService::sign(RSA* rsa,const QByteArray plain,int algorithm,bool base64){
    QByteArray signature;
    if(!rsa){
        return signature;
    }
    if(plain.isNull()||plain.isEmpty()){
        return signature;
    }

    auto plain_len = static_cast<unsigned int>(plain.count());
    auto plain_buffer = reinterpret_cast<const unsigned char*>(plain.data());
    auto rsa_size = static_cast<size_t>(RSA_size(rsa));
    auto sign_buffer = reinterpret_cast<unsigned char*>(malloc(rsa_size));
    memset(sign_buffer,0,rsa_size);
    unsigned int signLength = 0;

    auto ret = RSA_sign(algorithm,plain_buffer,plain_len,sign_buffer,&signLength,rsa);
    if(ret != 1){
        qCritical() << __FUNCTION__ << "could not sign: " << ERR_error_string(ERR_get_error(),nullptr);
        free(sign_buffer);
        return signature;
    }

    signature = QByteArray(reinterpret_cast<char*>(sign_buffer),static_cast<int>(signLength));
    free(sign_buffer);
    if(base64){signature = signature.toBase64(); }
    return signature;
}

bool QRsaService::verify(const QByteArray pub_pem,const QByteArray plain,const QByteArray sign,int algorithm){
    auto rsa = publicPem(pub_pem);
    auto flag = verify(rsa,plain,sign,algorithm);
    if(rsa){RSA_free(rsa);}
    return flag;
}

bool QRsaService::QRsaService::verify(RSA* rsa,const QByteArray plain,const QByteArray sign,int algorithm){
    if(!rsa){
        return false;
    }
    if(sign.isNull()||sign.isEmpty()){
        return false;
    }
    if(plain.isNull()||plain.isEmpty()){
        return false;
    }

    auto plain_len = static_cast<unsigned int>(plain.length());
    auto plain_buffer = reinterpret_cast<const unsigned char*>(plain.data());

    auto sign_length = static_cast<unsigned int>(sign.length());
    auto sign_buffer = reinterpret_cast<const unsigned char*>(sign.data());
    auto ret = RSA_verify(algorithm,plain_buffer,plain_len,sign_buffer,sign_length,rsa);
    if(ret != 1){
        qCritical() << __FUNCTION__ << "could not verify: " << ERR_error_string(ERR_get_error(),nullptr);
        return false;
    }
    return ret == 1;
}

bool QRsaService::publicPkcs1Pem(QByteArray content){
    return content.startsWith("-----BEGIN RSA PUBLIC KEY-----");
}

bool QRsaService::publicPkcs8Pem(QByteArray content){
    return content.startsWith("-----BEGIN PUBLIC KEY-----");
}

bool QRsaService::privatePkcs1Pem(QByteArray content){
    return content.startsWith("-----BEGIN RSA PRIVATE KEY-----");
}

bool QRsaService::privatePkcs8Pem(QByteArray content){
    return content.startsWith("-----BEGIN PRIVATE KEY-----");
}
