﻿#ifndef QAESSERVICE_H
#define QAESSERVICE_H

#include <QObject>
#include <QDebug>

#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/err.h>

class QAesService : public QObject
{
    Q_OBJECT
public:
    explicit QAesService(QObject *parent = nullptr);

    QByteArray encrypt(const QByteArray plain,const QByteArray key,const QByteArray iv,bool base64=true);

    QByteArray decrypt(const QByteArray cipher,const QByteArray key,const QByteArray iv);
};

#endif // QAESSERVICE_H
