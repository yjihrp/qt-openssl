下载 openssl https://www.openssl.org/

# msvc 编译 openssl

## 编译环境

### WIN 10 64 位

### VS 2019

### perl 5.32

下载 http://strawberryperl.com/

安装目录 C:\Strawberry

删除环境变量中的路径 C:\Strawberry\c

因为 Strawberry 套件中有 gcc、g++ ，为了后面编译 MINGW 版本，所以删除



## 编译命令

debug 64

```
手动创建目录 D:\work\openssl-1.1.1k\build\msvc\x64\debug

打开 vs 64 位命令行工具

# 进入 openssl 目录
cd /d D:\work\openssl-1.1.1k
# 配置 openssl 编译选项
perl Configure no-asm VC-WIN64A --debug --prefix=D:\work\openssl-1.1.1k\build\msvc\x64\debug --openssldir=D:\work\openssl-1.1.1k\build\msvc\x64\debug
# 编译
nmake
# 安装
nmake install
# 清理
nmake clean
```

release 64

```
手动创建目录 D:\work\openssl-1.1.1k\build\msvc\x64\release

打开 vs 64 位命令行工具

# 进入 openssl 目录
cd /d D:\work\openssl-1.1.1k
# 配置 openssl 编译选项
perl Configure no-asm VC-WIN64A --prefix=D:\work\openssl-1.1.1k\build\msvc\x64\release --openssldir=D:\work\openssl-1.1.1k\build\msvc\x64\release
# 编译
nmake
# 安装
nmake install
# 清理
nmake clean
```

debug 32

```
手动创建目录 D:\work\openssl-1.1.1k\build\msvc\x64\debug

打开 vs 64 位命令行工具

# 进入 openssl 目录
cd /d D:\work\openssl-1.1.1k
# 配置 openssl 编译选项
perl Configure no-asm VC-WIN32 --debug --prefix=D:\work\openssl-1.1.1k\build\msvc\x64\debug --openssldir=D:\work\openssl-1.1.1k\build\msvc\x64\debug
# 编译
nmake
# 安装
nmake install
# 清理
nmake clean
```

release 32

```
手动创建目录 D:\work\openssl-1.1.1k\build\msvc\x64\release

打开 vs 64 位命令行工具

# 进入 openssl 目录
cd /d D:\work\openssl-1.1.1k
# 配置 openssl 编译选项
perl Configure no-asm VC-WIN32 --prefix=D:\work\openssl-1.1.1k\build\msvc\x64\release --openssldir=D:\work\openssl-1.1.1k\build\msvc\x64\release
# 编译
nmake
# 安装
nmake install
# 清理
nmake clean
```




# mingw 编译 openssl

## 编译环境

### WIN 10 64 位

### QT 5.15.2

安装目录 C:\Qt5.15

安装 Qt 时，将 tools 中的 mingw32、mingw64 选中

已安装 Qt，用维护工具添加 mingw32、mingw64 组件

### msys2

下载 https://sourceforge.net/projects/msys2/

安装目录 C:\msys64

复制 C:\Qt5.15\Tools\mingw810_32 文件夹内所有文件到 C:\msys64\mingw32 文件夹

复制 C:\Qt5.15\Tools\mingw810_64 文件夹内所有文件到 C:\msys64\mingw64 文件夹

这样编译环境就和QT mingw 版本一样了

## 编译命令

debug 64

```
打开 MSYS2 MinGW 64-bit 命令行
# 创建目录
mkdir -p /d/work/openssl-1.1.1k/build/mingw/x64/debug
# 进入目录
cd /d/work/openssl-1.1.1k
# 配置 openssl 编译选项
./config no-asm --debug --prefix=/d/work/openssl-1.1.1k/build/mingw/x64/debug --openssldir=/d/work/openssl-1.1.1k/build/mingw/x64/debug
# 编译
mingw32-make
# 安装
mingw32-make install
# 清理
mingw32-make clean
```

release 64

```
打开 MSYS2 MinGW 64-bit 命令行
# 创建目录
mkdir -p /d/work/openssl-1.1.1k/build/mingw/x64/release
# 进入目录
cd /d/work/openssl-1.1.1k
# 配置 openssl 编译选项
./config no-asm --prefix=/d/work/openssl-1.1.1k/build/mingw/x64/release --openssldir=/d/work/openssl-1.1.1k/build/mingw/x64/release
# 编译
mingw32-make
# 安装
mingw32-make install
# 清理
mingw32-make clean
```

debug 32

```
打开 MSYS2 MinGW 32-bit 命令行
# 创建目录
mkdir -p /d/work/openssl-1.1.1k/build/mingw/x32/debug
# 进入目录
cd /d/work/openssl-1.1.1k
# 配置 openssl 编译选项
./config no-asm --debug --prefix=/d/work/openssl-1.1.1k/build/mingw/x32/debug --openssldir=/d/work/openssl-1.1.1k/build/mingw/x32/debug
# 编译
mingw32-make
# 安装
mingw32-make install
# 清理
mingw32-make clean
```

release 32

```
打开 MSYS2 MinGW 32-bit 命令行
# 创建目录
mkdir -p /d/work/openssl-1.1.1k/build/mingw/x32/release
# 进入目录
cd /d/work/openssl-1.1.1k
# 配置 openssl 编译选项
./config no-asm --prefix=/d/work/openssl-1.1.1k/build/mingw/x32/release --openssldir=/d/work/openssl-1.1.1k/build/mingw/x32/release
# 编译
mingw32-make
# 安装
mingw32-make install
# 清理
mingw32-make clean
```