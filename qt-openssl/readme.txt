源码
https://www.openssl.org/source/
编译
https://blog.csdn.net/ly7969/article/details/88139909
使用
https://blog.csdn.net/weixin_40512690/article/details/81359643
https://www.cnblogs.com/chevin/p/11041480.html

doc
https://blog.csdn.net/tuhuolong/article/details/42778945
https://www.cnblogs.com/mingzhang/p/9428964.html
https://www.cnblogs.com/cocoajin/p/6137651.html
https://blog.csdn.net/freesonWANG/article/details/87717361
https://www.cnblogs.com/cocoajin/p/6126099.html

VC-WIN32 是 32位版本，VC-WIN64A 对应 X64位版本，VC-WIN64I 对应 IA64 系统
no-shared 表示编译lib，去掉就是编译dll
no-asm 表示不使用汇编，
--prefix=C:\openssl 指定安装路径，--prefix和--openssldir用于输出
--debug去掉则为默认--release

debug
perl Configure no-asm VC-WIN64A --debug --prefix=D:\code\openssl-1.1.1d\build\debug --openssldir=D:\code\openssl-1.1.1d\build\debug

release
perl Configure no-asm VC-WIN64A --prefix=D:\code\openssl-1.1.1d\build\release --openssldir=D:\code\openssl-1.1.1d\build\release


debug
perl Configure no-asm VC-WIN32 --debug --prefix=D:\code\openssl-1.1.1d\build32\debug --openssldir=D:\code\openssl-1.1.1d\build32\debug

release
perl Configure no-asm VC-WIN32 --prefix=D:\code\openssl-1.1.1d\build32\release --openssldir=D:\code\openssl-1.1.1d\build32\release

nmake         编译
nmake test    编译后可以进行测试
nmake install  编译结果生成，这是最后一步
nmake clean    清理工作，先清理再编译


//******************************************************************************
1、PKCS#1
	public
		PEM_write_bio_RSAPublicKey
		PEM_read_bio_RSAPublicKey
			-----BEGIN RSA PUBLIC KEY-----
	private
		PEM_write_bio_RSAPrivateKey
			-----BEGIN RSA PRIVATE KEY-----
2、PKCS#8
	public
		PEM_write_bio_RSA_PUBKEY
		PEM_read_bio_RSA_PUBKEY
			-----BEGIN PUBLIC KEY-----
	private
			-----BEGIN PRIVATE KEY-----


RSA加密中的Padding
 
RSA_PKCS1_PADDING 填充模式，最常用的模式
要求: 输入：必须 比 RSA 钥模长(modulus) 短至少11个字节, 也就是　RSA_size(rsa) – 11 如果输入的明文过长，必须切割，然后填充。
输出：和modulus一样长
根据这个要求，对于1024bit的密钥，block length = 1024/8 – 11 = 117 字节

RSA_PKCS1_OAEP_PADDING
输入：RSA_size(rsa) – 41
输出：和modulus一样长

RSA_NO_PADDING　　不填充
输入：可以和RSA钥模长一样长，如果输入的明文过长，必须切割，　然后填充
输出：和modulus一样长
//******************************************************************************

cryptopp rsa
https://www.cryptopp.com/docs/ref/index.html
https://github.com/weidai11/cryptopp
https://www.coder4.com/archives/577

rsa
https://github.com/QuasarApp/Qt-Secret
botan rsa
https://botan.randombit.net/
https://github.com/randombit/botan

openssl之EVP系列
