﻿#ifndef QRSASERVICE_H
#define QRSASERVICE_H

#include <QObject>
#include <QFile>
#include <QDebug>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

class QRsaService : public QObject
{
    Q_OBJECT
public:
    explicit QRsaService(QObject *parent = nullptr);

    ///
    /// \brief generate
    /// \param pub
    /// \param pri
    /// \param bits
    /// \param pub_pem_ver 0 PKCS#1,1 PKCS#8
    /// \param pri_pem_ver 0 PKCS#1,1 PKCS#8 todo
    /// \param save
    /// \return
    ///
    bool generate(QByteArray& pub,QByteArray& pri,int bits,int pub_pem_ver=1,int pri_pem_ver=0,bool save=false);

    QByteArray pem(QString file);
    RSA* publicPem(QByteArray pem);
    RSA* privatePem(QByteArray pem);

    // 1 RSA_PKCS1_PADDING
    // 4 RSA_PKCS1_OAEP_PADDING
    QByteArray encrypt(const QByteArray pub_pem,const QByteArray plain,int padding=RSA_PKCS1_PADDING,bool base64=false);
    QByteArray encrypt(RSA* rsa,const QByteArray plain,int padding=RSA_PKCS1_PADDING,bool base64=false);

    // 1 RSA_PKCS1_PADDING
    // 4 RSA_PKCS1_OAEP_PADDING
    QByteArray decrypt(const QByteArray pri_pem,const QByteArray cipher,int padding=RSA_PKCS1_PADDING);
    QByteArray decrypt(RSA* rsa,const QByteArray cipher,int padding=RSA_PKCS1_PADDING);

    QByteArray sign(const QByteArray pri_pem,const QByteArray plain,int algorithm=NID_sha256,bool base64=false);
    QByteArray sign(RSA* rsa,const QByteArray plain,int algorithm=NID_sha256,bool base64=false);

    bool verify(const QByteArray pub_pem,const QByteArray plain,const QByteArray sign,int algorithm=NID_sha256);
    bool verify(RSA* rsa,const QByteArray plain,const QByteArray sign,int algorithm=NID_sha256);

private:
    bool publicPkcs1Pem(QByteArray content);
    bool publicPkcs8Pem(QByteArray content);
    bool privatePkcs1Pem(QByteArray content);
    bool privatePkcs8Pem(QByteArray content);
};

#endif // QRSASERVICE_H
