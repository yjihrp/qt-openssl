# 公用编译设置

CONFIG(debug,debug|release){
    MOC_DIR = debug/.moc
    RCC_DIR = debug/.rcc
    UI_DIR = debug/.ui
    OBJECTS_DIR = debug/.obj
}else{
    MOC_DIR = release/.moc
    RCC_DIR = release/.rcc
    UI_DIR = release/.ui
    OBJECTS_DIR = release/.obj
}

linux{
    # linux 设置依赖加载路径
    QMAKE_LFLAGS += -Wl,-rpath,\'\$\$ORIGIN/../lib:\$\$ORIGIN/lib:\$\$ORIGIN\'
}
