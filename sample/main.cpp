﻿#include <QFile>
#include <QDebug>
#include <QCoreApplication>

#include "qrsaservice.h"
#include "qaesservice.h"

void rsa_generate(){
    QRsaService service;

    QByteArray pub;
    QByteArray pri;
    int bits = 2048;
    bits = 4096;
    int pub_pem_ver = 1;
    int pri_pem_ver = 0;
    bool save = true;
    service.generate(pub,pri,bits,pub_pem_ver,pri_pem_ver,save);
}

void rsa_qt_key(bool b64=false,int padding=RSA_PKCS1_PADDING){
    QRsaService service;

    //    QByteArray pub,pri;
    //    if(!service.generate(pub,pri,2048,1,0,true)){
    //        qDebug() << "generate error";
    //        return;
    //    }

    auto text = QByteArray("hello world");
    qDebug() << __FUNCTION__ << "text" << text;

    auto pub_pem = service.pem("qt-public.pem");
    auto pri_pem = service.pem("qt-private.pem");

    // 1.encrypt
    auto cipher = service.encrypt(pub_pem,text,padding,b64);
    if(cipher.count() > 0){
        qDebug() << __FUNCTION__ << "cipher"<< cipher;
    }
    // 2.decrypt
    cipher = (b64 ? QByteArray::fromBase64(cipher):cipher);
    auto plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);


    // 3.sign
    auto algorithm = NID_sha512;
    auto sign = service.sign(pri_pem,text,algorithm,b64);
    if(sign.count() > 0){
        qDebug() << __FUNCTION__ << "sign" << sign;
    }
    // 4.verify
    sign = (b64 ? QByteArray::fromBase64(sign) : sign);
    auto clone = QByteArray("hello world");
    auto v = service.verify(pub_pem,clone,sign,algorithm);
    qDebug() << __FUNCTION__ << "verify" << v;
}

void rsa_js_key(bool b64=false,int padding=1){
    QRsaService service;
    auto text = QByteArray("hello world");
    qDebug() << __FUNCTION__ << "text" << text;

    QByteArray pub_pem = service.pem("js-public.pem");
    QByteArray pri_pem = service.pem("js-private.pem");

    // 1.encrypt
    auto cipher = service.encrypt(pub_pem,text,padding,b64);
    if(cipher.count() > 0){
        qDebug() << __FUNCTION__ << "cipher"<< cipher;
    }
    // 2.decrypt
    cipher = (b64 ? QByteArray::fromBase64(cipher):cipher);
    auto plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);


    // 3.sign
    auto algorithm = NID_sha512;
    auto sign = service.sign(pri_pem,text,algorithm,b64);
    if(sign.count() > 0){
        qDebug() << __FUNCTION__ << "sign" << sign;
    }
    // 4.verify
    sign = (b64 ? QByteArray::fromBase64(sign) : sign);
    auto clone = QByteArray("hello world");
    auto v = service.verify(pub_pem,clone,sign,algorithm);
    qDebug() << __FUNCTION__ << "verify" << v;
}

void rsa_qt_decrypt_js_data_by_qt_key(int padding=1) {
    QRsaService service;
    auto text = QByteArray("9clFkProzGqJCGP");
    qDebug() << __FUNCTION__ << "text" << text;
    QByteArray pri_pem = service.pem("qt-private.pem");
    auto cipher = QByteArray::fromBase64("l+zY9FRLf/gm7Odtdf/ta98I1xIrVIQEhFjL9Hi7mNWHa/OUImAurUfiVtMZzvp628GH6kt2X2ZqscyyBGfgJjAmq3jxjQ398PdAyOddimsZi+f/7Sj6CbV2Nx+LMI1ZzGtETA0Wf2rHS769O7kq1EhiF0Yzn6Po2ODpa7xSC+0MJQ/Hrh3QFkc5GrPQlYAXf7A+zFMWpAfcyGEVlyLs36/jMUyc6U/kqsmdjSMlpYIjCeRTk7EWpRF7zGFZsY7a0K8Qw7H0MpKW4sNv/IkVM3MAnHz7X2LrAq60vlSYuZV2l7Eb4fNw2tqfwM16Ges+EoUcolBd1ZqhGH0O2V0Ndw==");
    // success
    auto plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // error text != plain
    text = QByteArray("YJW8");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("l+zY9FRLf/gm7Odtdf/ta98I1xIrVIQEhFjL9Hi7mNWHa/OUImAurUfiVtMZzvp628GH6kt2X2ZqscyyBGfgJjAmq3jxjQ398PdAyOddimsZi+f/7Sj6CbV2Nx+LMI1ZzGtETA0Wf2rHS769O7kq1EhiF0Yzn6Po2ODpa7xSC+0MJQ/Hrh3QFkc5GrPQlYAXf7A+zFMWpAfcyGEVlyLs36/jMUyc6U/kqsmdjSMlpYIjCeRTk7EWpRF7zGFZsY7a0K8Qw7H0MpKW4sNv/IkVM3MAnHz7X2LrAq60vlSYuZV2l7Eb4fNw2tqfwM16Ges+EoUcolBd1ZqhGH0O2V0Ndw==");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // success
    text = QByteArray("YJW8");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("UxoAAoBgGHJAfkxslNjp5RJpm3ViBUlHZQq16UocSELYHlO+zyilLl60ItTVcfyqNMjkKvAt5X5sS1uHzjr3wDBPor6heCX3yF6HqN25/B+l7Dvr7sgMzFfTGgfN0vv0hKEx8xNRfgMzujvJnK8C5KAOMQX0xTWmDw+ptktBOM5HaPq68Hs8KlOjNWwIaOkmcKNc2L4HdeGlHq+EmkRs1xTQnYJnzkKkANOnCY/DlKu1uNfshhegbaqJIVbOw2Mipy0IJChNAuehlXF1DBQp/gHKD0aSLQXaARCogfHSNHjk6tGqpYQ8kB3pJU7EJNPa52ypKkbbahMTzWCNLDVY+g==");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // padding error
    text = QByteArray("YJW8");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("UxoAAoBgGHJAfkxslNjp5RJpm3ViBUlHZQq16UocSELYHlO+zyilLl60ItTVcfyqNMjkKvAt5X5sS1uHzjr3wDBPor6heCX3yF6HqN25/B+l7Dvr7sgMzFfTGgfN0vv0hKEx8xNRfgMzujvJnK8C5KAOMQX0xTWmDw+ptktBOM5HaPq68Hs8KlOjNWwIaOkmcKNc2L4HdeGlHq+EmkRs1xTQnYJnzkKkANOnCY/DlKu1uNfshhegbaqJIVbOw2Mipy0IJChNAuehlXF1DBQp/gHKD0aSLQXaARCogfHSNHjk6tGqpYQ8kB3pJU7EJNPa52ypKkbbahMTzWCNLDVY+g==");
    plain = service.decrypt(pri_pem,cipher,padding+1);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // pem error
    text = QByteArray("YJW8");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("UxoAAoBgGHJAfkxslNjp5RJpm3ViBUlHZQq16UocSELYHlO+zyilLl60ItTVcfyqNMjkKvAt5X5sS1uHzjr3wDBPor6heCX3yF6HqN25/B+l7Dvr7sgMzFfTGgfN0vv0hKEx8xNRfgMzujvJnK8C5KAOMQX0xTWmDw+ptktBOM5HaPq68Hs8KlOjNWwIaOkmcKNc2L4HdeGlHq+EmkRs1xTQnYJnzkKkANOnCY/DlKu1uNfshhegbaqJIVbOw2Mipy0IJChNAuehlXF1DBQp/gHKD0aSLQXaARCogfHSNHjk6tGqpYQ8kB3pJU7EJNPa52ypKkbbahMTzWCNLDVY+g==");
    pri_pem = service.pem("js-private.pem");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";
}

void rsa_qt_decrypt_js_data_by_js_key(int padding=1) {
    QRsaService service;
    auto text = QByteArray("00J3fh19IVgtODmqGxv");
    qDebug() << __FUNCTION__ << "text" << text;
    QByteArray pri_pem = service.pem("js-private.pem");
    auto cipher = QByteArray::fromBase64("yqB6qfV0HmDcW7ZcJDRsnNUT99iVdKcVO4tWTPEhPhbISD9upkM6YjiUBa6OIiT/gdPYrJmPEFbxgh7C8iQwmEh5EImwMhrgvHKsQrftSDMBtw6CoZSgT1oBRMHXSiNFKOxummj7O0tYxGeF55qsu6s6HdKxwlmFizMXG906Lxfdy3C4QL3wMhKMYiFRdeqh7Vz31a1yox1Urpd0A7EPxW9cp3dKxV10cx5XZGlDknBdMI+4TpeedpORNl0FG5ScQnRs+0g/pBuYD80BpBMhNir08nyAyaP1ota2zVWK3rpanOIcQY7dCS4kO/97vfrJkZmXDRrLEsitbCELaTAyHQ==");
    // success
    auto plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // error text != plain
    text = QByteArray("vJ7rC4DiJqeNo2jghO37vbJJwgQhHnzoZTSk50Czzxuek7p");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("yqB6qfV0HmDcW7ZcJDRsnNUT99iVdKcVO4tWTPEhPhbISD9upkM6YjiUBa6OIiT/gdPYrJmPEFbxgh7C8iQwmEh5EImwMhrgvHKsQrftSDMBtw6CoZSgT1oBRMHXSiNFKOxummj7O0tYxGeF55qsu6s6HdKxwlmFizMXG906Lxfdy3C4QL3wMhKMYiFRdeqh7Vz31a1yox1Urpd0A7EPxW9cp3dKxV10cx5XZGlDknBdMI+4TpeedpORNl0FG5ScQnRs+0g/pBuYD80BpBMhNir08nyAyaP1ota2zVWK3rpanOIcQY7dCS4kO/97vfrJkZmXDRrLEsitbCELaTAyHQ==");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // success
    text = QByteArray("vJ7rC4DiJqeNo2jghO37vbJJwgQhHnzoZTSk50Czzxuek7p");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("tn0SQ7hwD8hG8djn6wsuoc9fyD3iAT/XGuAX41WQHZieNgHKCp7d3lEB7nDV4PTxffgiVfs7Ii4KzPxKgIPItFPbCFchUTrZb01tHafAqKubv96BAkDIsRRFV3vZnSTWVe7nPklavv2y3uXBB2JNrTcVg6cMNYzjr51oMlpQatrSmkJoZ4CfxBu0aIMmFRN/PqnZD7+HJOu6+F5loQbBCG/qfwEVU8qT86yVa1SFms76oT7NxrHHROWRv+LcPQfG1FXTrTsqReoYxld0GnWTVw12uEEUd7Q7ik5narzObFYkmWNLdqFKxY6QQ+9SSNr52aOjCAAeYsm8v1cGPKSWDw==");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // padding error
    text = QByteArray("vJ7rC4DiJqeNo2jghO37vbJJwgQhHnzoZTSk50Czzxuek7p");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("tn0SQ7hwD8hG8djn6wsuoc9fyD3iAT/XGuAX41WQHZieNgHKCp7d3lEB7nDV4PTxffgiVfs7Ii4KzPxKgIPItFPbCFchUTrZb01tHafAqKubv96BAkDIsRRFV3vZnSTWVe7nPklavv2y3uXBB2JNrTcVg6cMNYzjr51oMlpQatrSmkJoZ4CfxBu0aIMmFRN/PqnZD7+HJOu6+F5loQbBCG/qfwEVU8qT86yVa1SFms76oT7NxrHHROWRv+LcPQfG1FXTrTsqReoYxld0GnWTVw12uEEUd7Q7ik5narzObFYkmWNLdqFKxY6QQ+9SSNr52aOjCAAeYsm8v1cGPKSWDw==");
    plain = service.decrypt(pri_pem,cipher,padding + 1);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";

    // pem error
    text = QByteArray("vJ7rC4DiJqeNo2jghO37vbJJwgQhHnzoZTSk50Czzxuek7p");
    qDebug() << __FUNCTION__ << "text" << text;
    cipher = QByteArray::fromBase64("tn0SQ7hwD8hG8djn6wsuoc9fyD3iAT/XGuAX41WQHZieNgHKCp7d3lEB7nDV4PTxffgiVfs7Ii4KzPxKgIPItFPbCFchUTrZb01tHafAqKubv96BAkDIsRRFV3vZnSTWVe7nPklavv2y3uXBB2JNrTcVg6cMNYzjr51oMlpQatrSmkJoZ4CfxBu0aIMmFRN/PqnZD7+HJOu6+F5loQbBCG/qfwEVU8qT86yVa1SFms76oT7NxrHHROWRv+LcPQfG1FXTrTsqReoYxld0GnWTVw12uEEUd7Q7ik5narzObFYkmWNLdqFKxY6QQ+9SSNr52aOjCAAeYsm8v1cGPKSWDw==");
    pri_pem = service.pem("qt-private.pem");
    plain = service.decrypt(pri_pem,cipher,padding);
    if(plain.count() > 0){
        qDebug() << __FUNCTION__ << "plain" << plain;
    }
    qDebug() << __FUNCTION__ << "encrypt,decrypt" << (plain == text);
    qDebug() << "\n";
}

void aes_qt(){
    auto key = QByteArray("CHPF1xk7OBfkGIqLoq6VbE0s5DXawHsF");
    auto iv = QByteArray("JSd4a3GCJOHx0Wfr");
    auto text = QByteArray("4aYWdb4DDJsD3gufecOpFi1ZdQ5eyzZAMVRrrnOZg2fnNqG5PpZTCjsxGq");
    qDebug() << __FUNCTION__ << "text" << text;
    QAesService service;
    auto cipher = service.encrypt(text,key,iv,false);
    qDebug() << __FUNCTION__ << "cipher" << cipher;

    auto plain = service.decrypt(cipher,key,iv);
    qDebug() << __FUNCTION__ << "plain" << plain;
}

void aes_qt_decrypt_js_data(){
    auto key = QByteArray("CHPF1xk7OBfkGIqLoq6VbE0s5DXawHsF");
    auto iv = QByteArray("JSd4a3GCJOHx0Wfr");
    QAesService service;
    auto cipher = QByteArray::fromBase64("zboUGYiCBBlyuee+hGD57e83nmF1jgzdNVRNRgK93ejtR4OVoQPbCFptChnLu+gJSd9o1Z50pmf6hXXEwzGPxg==");
    auto plain = service.decrypt(cipher,key,iv);
    qDebug() << __FUNCTION__ << "plain" << plain;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    rsa_generate();

    //    bool b64 = true;
    //    // 1 RSA_PKCS1_PADDING
    //    // 4 RSA_PKCS1_OAEP_PADDING
    //    int padding = RSA_PKCS1_PADDING;
    //    rsa_qt_key(b64,padding);
    //    rsa_js_key(b64,padding);

    //    rsa_qt_decrypt_js_data_by_qt_key();
    //    rsa_qt_decrypt_js_data_by_js_key();

    //    aes_qt();
    //    aes_qt_decrypt_js_data();

    qDebug() << "end";

    a.exit(0);
    return 0;
}
