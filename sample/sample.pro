QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

include($$PWD/../compile/compile.pri)
include($$PWD/../qt-openssl/qt-openssl.pri)

SOURCES += main.cpp
