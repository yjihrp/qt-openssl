# qt-openssl


#### 介绍
项目由 Qt 程序 和 JS 程序组成

1、Qt 程序封装 openssl 的 RSA、AES 加密、解密;测试 JS RSA 加密，Qt 解密

2、JS 程序测试了 JS 版本的加密库  jsencrypt、rsa-javascript;测试 JS 解密，Qt 加密

#### 目录结构
| 路径               | 说明                                                         |
| ------------------ | ------------------------------------------------------------ |
| compile            | 编译项目设置                                                 |
| qt-openssl         | QT 封装 openssl                                              |
| ------ mingw       | mingw x32、x64 openssl, [如何编译 openssl 1.1.1k](qt-openssl/win10 mingw 及 vs 编译 openssl 1.1.1k.md) |
| ------ msvc        | msvc 2019 x32、x64 openssl [如何编译 openssl 1.1.1k](qt-openssl/win10 mingw 及 vs 编译 openssl 1.1.1k.md) |
| ------ aarch       |                                                              |
| ------ amd         |                                                              |
| ------ loongarch64 |                                                              |
| ------ mips        |                                                              |
| sample             | 样例                                                         |
| ------ html        | html RAS 程序,html 程序使用 npm 管理安装包                   |
| ------ key         | 测试 RAS 样例 key                                            |
| ------ main.cpp    | QT 测试程序                                                  |


#### 环境要求

1.  QT 5.15
2.  nodejs

#### 使用说明

1.  sample/html/index.html 测试 JS RSA 加密库 jsencrypt、rsa-javascript
2.  sample/html/qt_rsa.html 测试 JS 解密、QT 加密
3.  main.cpp 测试 openssl RSA 加密、解密;QT openssl 解密JS加密;openssl AES 加密、解密
